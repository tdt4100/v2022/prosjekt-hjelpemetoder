package exampleproject;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import util.StageContainer;

import java.io.IOException;

public class ExamplePopupApp extends Application {

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        primaryStage.setTitle("Example Popup App");

        FXMLLoader loader = new FXMLLoader(getClass().getResource("App1.fxml"));

        try {
            primaryStage.setScene(new Scene(loader.load()));
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        if (loader.getController() instanceof StageContainer) {
            ((StageContainer) loader.getController()).setStage(primaryStage);
        }

        primaryStage.show();
    }
}
